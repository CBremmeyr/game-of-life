///
/// Author: Corbin Bremmeyr
/// Date: 23 February 2020
///
/// Conway's Game of Life with a CLI

use std::{thread, time};
use std::io::{self, Write};
use std::fmt;
use std::str::FromStr;
use rand::{thread_rng, Rng};

/// Struct to hold data for game's board.
/// Uses 1-D array with index calulations to act like a 2-D array.
struct BoardState {
    cells: Vec<bool>,
    width: usize,
    height: usize,
}

impl BoardState {

    /// Allocate memeory for BoardState struct
    fn new(width: usize, height: usize) -> BoardState {
        BoardState {
            cells: vec![false; width * height],
            width,
            height,
        }
    }

    /// Get a cell's value from the board
    ///
    /// param x - first index value
    /// param y - second index value
    ///
    /// return value of cell at position (x, y)
    fn get_cell(&self, x: usize, y: usize) -> bool {
        self.cells[self.width * y + x]
    }

    /// Set a cell's value in the board
    ///
    /// param x - first index value
    /// param y - second index value
    /// param cell - value to set cell (x, y) to
    fn set_cell(&mut self, x: usize, y: usize, cell: bool) {
        self.cells[self.width * y + x] = cell;
    }

    /// Randomly populate a new board
    ///
    /// param width - width of the new board
    /// param height - height of the new baord
    /// seed_count - number of live cells to place on board
    ///
    /// return board will size `width' x `height' with `seed_count' live cells
    /// if seed_count is too large then empty board is returned
    fn populate(width: usize, height: usize, seed_count: usize) -> BoardState {
        let mut board = BoardState::new(width, height);
        let mut rng = thread_rng();
        let mut count = 0;

        if seed_count > (width * height) {return board }

        // Initialize board population with `seed_count' live cells
        while count < seed_count {
            let n: usize = rng.gen_range(0, width);
            let m: usize = rng.gen_range(0, height);

            if !board.get_cell(n, m) {
                board.set_cell(n, m, true);
                count += 1;
            }
        }

        board
    }

    /// Iterate board to next generation in accordance to game's rules
    ///
    /// returns board after next generation
    fn next_generation(&self) -> BoardState {
        let mut board = BoardState::new(self.width, self.height);

        // Look at old board and make changes to new board
        for y in 0..self.height {
            for x in 0..self.width {

                // Get neighbour count for current cell
                let count = self.neighbour_count(x, y);

                // Rule 1: Any live cell with fewer than two live neighbours dies
                if count < 2 {
                    board.set_cell(x, y, false);
                }

                // Rule 2: Any live cell with two or three live neighbours lives
                if self.get_cell(x, y) && count == 2 || count == 3 {
                    board.set_cell(x, y, true);
                }

                // Rule 3: Any live cell with more than three live neighbours dies
                if count > 3 {
                    board.set_cell(x, y, false);
                }

                // Rule 4: Any dead cell with three live neighbours becomes a live cell
                if count == 3 && !self.get_cell(x, y) {
                    board.set_cell(x, y, true);
                }
            }
        }

        board
    }

    /// Find number of live adjacent cells
    ///
    /// param x - first index of cell
    /// param y - second index of cell
    ///
    /// return number of adjacent cells to cell (x, y)
    fn neighbour_count(&self, x: usize, y: usize) -> u32 {
        let mut count: u32 = 0;

        // Get neighbourhood's index bounds
        let lower_x = if x > 0 {x - 1} else {0};
        let lower_y = if y > 0 {y - 1} else {0};
        let upper_x = if x+1 < self.width {x + 1} else {x};
        let upper_y = if y+1 < self.height {y + 1} else {y};

        // Count number of live cells in neighbourhood of (x, y)
        for i in lower_x..=upper_x {
            for j in lower_y..=upper_y {
                count = if self.get_cell(i, j) {count + 1} else {count};
            }
        }

        // Correct for including current cell in neighbour count
        if self.get_cell(x, y) && count > 0 {
            count - 1
        }
        else {
            count
        }
    }

    /// Check if all cells on the board are dead
    ///
    /// return true if all cells are dead, else false
    fn all_dead(&self) -> bool {
        for i in &self.cells {
            if *i {
                return false;
            }
        }
        true
    }

    /// Check if two BoardStates have the same values
    ///
    /// param board - BoardState to compare `self' to
    ///
    /// return true if `self' and `baord' have the same values
    fn equals(&self, board: &BoardState) -> bool {
        if self.width != board.width ||
           self.height != board.height {

            return false;
        }
        else {
            for y in 0..self.height {
                for x in 0..self.width {
                    if self.get_cell(x, y) != board.get_cell(x, y) {
                        return false;
                    }
                }
            }
        }

        true
    }
}

impl fmt::Display for BoardState {

    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {

        // Output top of box
        for _i in 0..(self.width + 2) {
            write!(f, "-")?;
        }
        writeln!(f)?;

        // Output board's cells
        for y in 0..self.height {
            write!(f, "|")?;
            for x in 0..self.width {
                if self.get_cell(x, y) {
                    write!(f, "X")?;
                }
                else {
                    write!(f, " ")?;
                }
            }
            writeln!(f, "|")?;
        }

        // Output bottom of box
        for _i in 0..(self.width + 2) {
            write!(f, "-")?;
        }

        Ok(())
    }
}

fn main() {

    // Delay between generations (ms)
    let delay: u64 = 1000;


    // Print welcome message and program discription
    welcome();

    // Get initial values
    let width: usize = get_input("Enter a width:");
    println!("Width: {}", width);
    let height: usize = get_input("Enter a height:");
    println!("Height: {}", height);
    let seed_count: usize = get_input("Enter a starting population:");
    println!("Starting population: {}", seed_count);

    // Check if board size entered was 0 x 0
    let mut board = if width != 0 && height != 0 {
        BoardState::populate(width, height, seed_count)
    }
    else {
        BoardState::new(50, 10)
    };

    if board.all_dead() {
        board.set_cell(1, 0, true);
        board.set_cell(2, 1, true);
        board.set_cell(0, 2, true);
        board.set_cell(1, 2, true);
        board.set_cell(2, 2, true);
    }

    loop {
        println!("{}", board);
        let old_board = board;
        board = old_board.next_generation();
        thread::sleep(time::Duration::from_millis(delay));

        // Check if all cells are dead or all cells are static
        if board.all_dead() || board.equals(&old_board) {
            println!("{}", board);
            return;
        }
    }
}

/// Print prompt and get user input until input is properly parsed to type `T'
///
/// param prompt - string to be printed to stdio
///
/// return user input parsed into type `T'
fn get_input<T: FromStr>(prompt: &str) -> T {

    let mut input = String::new();

    // Loop until input is able to be parsed into type `T'
    loop {

        // Print prompt and get input
        print!("{}", prompt);
        io::stdout().flush().expect("ERROR: failed to flush stdout");
        match io::stdin().read_line(&mut input) {
           Ok(_n) => {}
            Err(error) => println!("error: {}", error),
        }

        // Try to parse input, if parsed then return it
        match input.trim_end().parse::<T>() {
            Ok(n) => return n,
            Err(_) => {
                input.clear();
            }
        };
    }
}

/// Print welcome message and program description
fn welcome() {
    println!("--- Conway's Game of Life ---");
    println!("Enter size of board and starting number of lives cells. \
Live cells will be randomly placed. Enter all zeros for the \
size and board will start with a glider.");
}

